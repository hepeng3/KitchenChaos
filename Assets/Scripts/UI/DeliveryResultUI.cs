using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeliveryResultUI : MonoBehaviour
{
    [SerializeField] private Image background;
    [SerializeField] private TextMeshProUGUI resultText;
    [SerializeField] private Image icon;
    [SerializeField] private Color successColor;
    [SerializeField] private Color failedColor;
    [SerializeField] private Sprite successIcon;
    [SerializeField] private Sprite failedIcon;
    // Start is called before the first frame update
    void Start()
    {
        DeliveryManager.Instance.OnDeliverySucceeded += Instance_OnDeliverySucceeded;
        DeliveryManager.Instance.OnDeliveryFailed += Instance_OnDeliveryFailed;
        Hide();
    }

    private void Instance_OnDeliveryFailed(object sender, System.EventArgs e)
    {
        Show();
        background.color = failedColor;
        icon.sprite = failedIcon;
        resultText.text = "DELIVERY\nFAILED";
        Invoke("Hide", 1.0f);
    }

    private void Instance_OnDeliverySucceeded(object sender, System.EventArgs e)
    {
        Show();
        background.color = successColor;
        icon.sprite = successIcon;
        resultText.text = "DELIVERY\nSUCCESS";
        Invoke("Hide", 1.0f);
    }
    private void Show()
    {
        gameObject.SetActive(true);
    }
    private void Hide()
    {
        gameObject.SetActive(false);
    }
}
