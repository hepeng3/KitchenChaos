using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarWarningUI : MonoBehaviour
{
    [SerializeField] private StoveCounter stoveCounter;
    private Animator animator;
    private const string IS_FLASHING = "IsFlashing";
    // Start is called before the first frame update
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    void Start()
    {
        stoveCounter.OnProgressUpdate += StoveCounter_OnProgressUpdate;
    }

    private void StoveCounter_OnProgressUpdate(object sender, IHasProgress.OnProgressUpdateEventArgs e)
    {
        if (e.progressNormalized >= .5f && stoveCounter.isBurning())
        {
            animator.SetBool(IS_FLASHING, true);
        }
        else
        {
            animator.SetBool(IS_FLASHING, false);
        }
    }
}
