using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryCounter : BaseCounter
{
    public override void Interact(Player player)
    {
        PlateKitchenObject plateKitchenObject;
        if(player.HasKitchenObject() && player.GetKitchenObject().TryGetPlateKitchenObject(out plateKitchenObject))
        {
            DeliveryManager.Instance.DeliveryRecipe(plateKitchenObject.GetIngredientList());
            plateKitchenObject.DestroySelf();
        }
    }
}
