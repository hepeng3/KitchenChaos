using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DeliveryRecipeContainerUI : MonoBehaviour
{
    [SerializeField] private RectTransform recipeIconTemplate;
    [SerializeField] private RectTransform recipeIconContainer;
    [SerializeField] private TextMeshProUGUI recipeTitle;
    public void SetSingleRecipe(RecipeSO recipeSO)
    {
        recipeTitle.text = recipeSO.recipeName;
        foreach(RectTransform child in recipeIconContainer)
        {
            if (child != recipeIconTemplate)
            {
                Destroy(child.gameObject);
            }
        }
        foreach(KitchenObjectSO kitchenObjectSO in recipeSO.kitchenObjectSOList)
        {
            RectTransform recipeIcon = Instantiate(recipeIconTemplate, recipeIconContainer);
            recipeIcon.gameObject.SetActive(true);
            recipeIcon.GetComponent<Image>().sprite = kitchenObjectSO.sprite;
        }
    }
}
