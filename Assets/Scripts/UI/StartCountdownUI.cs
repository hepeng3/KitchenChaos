using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StartCountdownUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI countdownText;
    private int lastCountdownNumber;

    private void Start()
    {
        KitchenChaosGameManager.Instance.OnWaitingToCountdown += Instance_OnWaitingToCountdown;
        Hide();
    }

    private void Instance_OnWaitingToCountdown(object sender, System.EventArgs e)
    {
        Show();
    }

    void Update()
    {
        int countdownNumber = KitchenChaosGameManager.Instance.GetCountdownNumber();
        if (countdownNumber <= 0)
        {
            Hide();
        }
        else
        {
            countdownText.text = countdownNumber.ToString();
        }

        if (countdownNumber != lastCountdownNumber && countdownNumber > 0)
        {
            SoundManager.Instance.PlayWarningSound(Vector3.zero);
            lastCountdownNumber = countdownNumber;
        }
    }
    private void Hide()
    {
        gameObject.SetActive(false);
    }
    private void Show()
    {
        gameObject.SetActive(true);
    }
}
