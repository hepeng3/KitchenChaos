using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameOverUI : MonoBehaviour
{
    [SerializeField] private Button playAgainButton;
    [SerializeField] private TextMeshProUGUI recipeDeliveredText;
    // Start is called before the first frame update
    private void Awake()
    {
        playAgainButton.onClick.AddListener(() =>
        {
            Loader.Load(Loader.Scene.MainMenuScene);
        });
    }
    void Start()
    {
        KitchenChaosGameManager.Instance.OnGameOver += GameManagerInstance_OnGameOver;
        Hide();
    }

    private void GameManagerInstance_OnGameOver(object sender, System.EventArgs e)
    {
        recipeDeliveredText.text = DeliveryManager.Instance.GetSuccessfulDeliveryRecipeAmount().ToString();
        Show();
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
    private void Show()
    {
        gameObject.SetActive(true);
    }
}
