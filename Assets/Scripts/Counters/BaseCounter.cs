using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseCounter : MonoBehaviour, IKitchenObjectParent
{
    [SerializeField] private Transform counterTopPoint;
    private KitchenObject kitchenObject;
    public static event EventHandler OnObjectPlaced;
    // Start is called before the first frame update
    public virtual void Interact(Player player)
    {
        Debug.Log("counter interact");
    }
    public virtual void InteractAlternate(Player player)
    {
        Debug.Log("counter interact alternate");
    }
    public Transform GetKitchenObjectFollowPoint()
    {
        return counterTopPoint;
    }
    public void SetKitchenObject(KitchenObject kitchenObject)
    {
        this.kitchenObject = kitchenObject;
        if (this.kitchenObject != null)
        {
            OnObjectPlaced?.Invoke(this, EventArgs.Empty);
        }
    }
    public void ClearKitchenObject()
    {
        this.kitchenObject = null;
    }
    public bool HasKitchenObject()
    {
        return this.kitchenObject != null;
    }
    public KitchenObject GetKitchenObject()
    {
        return this.kitchenObject;
    }
}
