using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipeLogo : MonoBehaviour
{
    [SerializeField] private PlateKitchenObject plateKitchenObject;
    [SerializeField] private RectTransform panel;
    [SerializeField] private RectTransform recipeLogoTemplate;
    // Start is called before the first frame update
    void Start()
    {
        plateKitchenObject.OnAddIngredient += PlateKitchenObject_OnAddIngredient;
    }

    private void PlateKitchenObject_OnAddIngredient(object sender, PlateKitchenObject.OnAddIngredientEventArgs e)
    {
        RectTransform recipeLogo = Instantiate(recipeLogoTemplate, panel);
        recipeLogo.gameObject.SetActive(true);
        recipeLogo.GetComponent<SingleRecipeLogo>().SetRecipeLogo(e.kitchenObjectSO);
    }
}
