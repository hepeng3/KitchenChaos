using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class StoveCounterVisual : MonoBehaviour
{
    [SerializeField] private StoveCounter stoveCounter;
    [SerializeField] private GameObject SizzlingParticles;
    [SerializeField] private GameObject StoveOnVisual;
    // Start is called before the first frame update
    void Start()
    {
        stoveCounter.OnStateUpate += StoveCounter_OnStateUpate;
    }

    private void StoveCounter_OnStateUpate(object sender, StoveCounter.OnStateUpateEventArgs e)
    {
        if (e.updatedState == StoveCounter.State.Frying || e.updatedState == StoveCounter.State.Burning)
        {
            SizzlingParticles.SetActive(true);
            StoveOnVisual.SetActive(true);
        }
        else
        {
            SizzlingParticles.SetActive(false);
            StoveOnVisual.SetActive(false);
        }
    }
}
