using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateCounter : BaseCounter
{
    [SerializeField] private KitchenObjectSO plateKitchenObjectSO;

    public event EventHandler OnPlateSpawned;
    public event EventHandler OnPlateRemoved;

    private float spawnPlateCountdown;
    private float spawnPlateCountdownMax = 3f;
    private int spawnPlateCount;
    private int spawnPlateCountMax = 4;

    private void Update()
    {
        if (KitchenChaosGameManager.Instance.IsPlaying())
        {
            spawnPlateCountdown -= Time.deltaTime;
            if (spawnPlateCountdown <= 0f && spawnPlateCount < spawnPlateCountMax)
            {
                spawnPlateCountdown = spawnPlateCountdownMax;

                OnPlateSpawned?.Invoke(this, EventArgs.Empty);

                spawnPlateCount++;
            }
        }
    }
    public override void Interact(Player player)
    {
        if (!player.HasKitchenObject() && spawnPlateCount > 0)
        {
            KitchenObject.SpawnKitchenObject(plateKitchenObjectSO, player);

            OnPlateRemoved?.Invoke(this, EventArgs.Empty);

            spawnPlateCount--;
        }
    }
}
