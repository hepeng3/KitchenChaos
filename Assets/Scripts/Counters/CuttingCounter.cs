using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingCounter : BaseCounter, IHasProgress
{
    [SerializeField] private CuttingRecipeSO[] cuttingRecipeSOArray;
    private CuttingRecipeSO currentRecipeSO;
    private int cuttingProgress;

    public event EventHandler<IHasProgress.OnProgressUpdateEventArgs> OnProgressUpdate;
    public event EventHandler OnCutAny;
    public static event EventHandler OnCut;

    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            if (player.HasKitchenObject())
            {
                CuttingRecipeSO cuttingRecipeSO;
                if (HasRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO(), out cuttingRecipeSO))
                {
                    player.GetKitchenObject().SetKitchenObjectParent(this);
                    currentRecipeSO = cuttingRecipeSO;

                    cuttingProgress = 0;
                    OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                    {
                        progressNormalized = (float)cuttingProgress / currentRecipeSO.cuttingProgressMax
                    });
                }
            }
        }
        else
        {
            if (!player.HasKitchenObject())
            {
                GetKitchenObject().SetKitchenObjectParent(player);
                ClearCurrentRecipe();
            }
            else
            {
                PlateKitchenObject plateKitchenObject;
                if(player.GetKitchenObject().TryGetPlateKitchenObject(out plateKitchenObject))
                {
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject()))
                    {
                        GetKitchenObject().DestroySelf();
                    }
                }
            }
        }
    }

    public override void InteractAlternate(Player player)
    {
        if (HasKitchenObject() && currentRecipeSO != null)
        {
            cuttingProgress++;
            OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
            {
                progressNormalized = (float)cuttingProgress / currentRecipeSO.cuttingProgressMax
            });

            if (cuttingProgress >= currentRecipeSO.cuttingProgressMax)
            {
                GetKitchenObject().DestroySelf();
                KitchenObject.SpawnKitchenObject(currentRecipeSO.output, this);
                ClearCurrentRecipe();
            }

            OnCutAny?.Invoke(this, EventArgs.Empty);
            OnCut?.Invoke(this, EventArgs.Empty);
        }
    }
    private CuttingRecipeSO GetCuttingRecipeSOWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        foreach(CuttingRecipeSO cuttingRecipeSO in cuttingRecipeSOArray)
        {
            if(cuttingRecipeSO.input == inputKitchenObjectSO)
            {
                return cuttingRecipeSO;
            }
        }
        return null;
    }
    private bool HasRecipeWithInput(KitchenObjectSO inputKitchenObjectSO, out CuttingRecipeSO cuttingRecipeSO)
    {
        cuttingRecipeSO = GetCuttingRecipeSOWithInput(inputKitchenObjectSO);
        return cuttingRecipeSO != null;
    }
    private void ClearCurrentRecipe()
    {
        if (currentRecipeSO != null)
        {
            currentRecipeSO = null;
        }
    }
}
