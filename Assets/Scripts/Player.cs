using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IKitchenObjectParent
{
    [SerializeField] private GameInput gameInput;
    [SerializeField] private Transform kitchenObjectHoldPoint;
    public static Player Instance;

    public bool isWalking;

    private float movementSpeed = 6f;
    private float rotationSpeed = 5f;

    private float castRadius = .7f;
    private float castHeight = 2f;
    private float maxDistance = .1f;

    private BaseCounter selectedCounter;
    private KitchenObject kitchenObject;
    private Vector3 lastInteractDir;

    public event EventHandler OnPickObject;
    public event EventHandler<OnSelectedCounterChangedEventArgs> OnSelectedCounterChanged;
    public class OnSelectedCounterChangedEventArgs: EventArgs
    {
        public BaseCounter selectedCounter;
        public OnSelectedCounterChangedEventArgs(BaseCounter baseCounter)
        {
            this.selectedCounter = baseCounter;
        }
    }

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        gameInput.OnInteractPerformed += GameInput_OnInteractPerformed;
        gameInput.OnInteractAlternatePerformed += GameInput_OnInteractAlternatePerformed;
    }

    private void GameInput_OnInteractAlternatePerformed(object sender, EventArgs e)
    {
        if (selectedCounter != null && KitchenChaosGameManager.Instance.IsPlaying())
        {
            selectedCounter.InteractAlternate(this);
        }
    }

    private void GameInput_OnInteractPerformed(object sender, EventArgs e)
    {
        Debug.Log("interact performed");
        if (selectedCounter != null && KitchenChaosGameManager.Instance.IsPlaying())
        {
            selectedCounter.Interact(this);
        }
    }

    // Update is called once per frame
    void Update()
    {
        HandleMovement();
        HandleInteractions();
    }

    private void HandleMovement()
    {
        Vector2 movementVector = gameInput.GetMovementVector();

        Vector3 moveDir = new Vector3(movementVector.x, 0, movementVector.y);

        RaycastHit hitInfo;

        bool canMove = !Physics.CapsuleCast(transform.position, transform.position + transform.up * castHeight, castRadius, moveDir, out hitInfo, maxDistance);

        if (!canMove)
        {
            if (moveDir.x < -.5f || moveDir.x > .5f)
            {
                Vector3 tempDir = new Vector3(moveDir.x, 0, 0).normalized;
                canMove = !Physics.CapsuleCast(transform.position, transform.position + transform.up * castHeight, castRadius, tempDir, out hitInfo, maxDistance);
                if (canMove)
                {
                    moveDir = tempDir;
                }
                else
                {
                    tempDir = new Vector3(0, 0, moveDir.z).normalized;
                    canMove = !Physics.CapsuleCast(transform.position, transform.position + transform.up * castHeight, castRadius, tempDir, out hitInfo, maxDistance);
                    if (canMove)
                    {
                        moveDir = tempDir;
                    }
                }
            }
        }

        if (canMove)
        {
            transform.position += moveDir * movementSpeed * Time.deltaTime;
        }

        isWalking = moveDir != Vector3.zero;

        transform.forward = Vector3.Slerp(transform.forward, moveDir, rotationSpeed * Time.deltaTime);
    }

    // 选定counter
    // 接收交互输入时，发出player交互事件（包含选定的counter，以及交互输入的信息）
    private void HandleInteractions()
    {
        // 选定counter更新状态机
        // 无选定counter=>选定counter
        // 有选定counter=>选定新counter
        // 有选定counter=>无选定counter
        Vector2 movementVector = gameInput.GetMovementVector();

        Vector3 moveDir = new Vector3(movementVector.x, 0, movementVector.y);

        if (moveDir != Vector3.zero)
        {
            lastInteractDir = moveDir;
        }

        RaycastHit raycastHit;
        //raycast？
        if (Physics.CapsuleCast(transform.position, transform.position + transform.up * castHeight, castRadius, lastInteractDir, out raycastHit, maxDistance))
        {
            //TryGetComponent?
            BaseCounter currentCounter = raycastHit.transform.GetComponent<BaseCounter>();
            if (currentCounter != selectedCounter)
            {
                SetSelectedCounter(currentCounter);
            }
        }
        else
        {
            if (selectedCounter != null)
            {
                SetSelectedCounter(null);
            }
        }
    }

    private void SetSelectedCounter(BaseCounter selectedCounter)
    {
        // counter视觉对象变化规则：由counter自行处理，player只触发选定counter变动事件
        this.selectedCounter = selectedCounter;

        OnSelectedCounterChanged?.Invoke(this, new OnSelectedCounterChangedEventArgs(selectedCounter));
    }
    public Transform GetKitchenObjectFollowPoint()
    {
        return kitchenObjectHoldPoint;
    }
    public bool HasKitchenObject()
    {
        return kitchenObject != null;
    }
    public void SetKitchenObject(KitchenObject kitchenObject)
    {
        this.kitchenObject = kitchenObject;

        if (this.kitchenObject != null)
        {
            OnPickObject?.Invoke(this, EventArgs.Empty);
        }
    }
    public KitchenObject GetKitchenObject()
    {
        return this.kitchenObject;
    }
    public void ClearKitchenObject()
    {
        this.kitchenObject = null;
    }
}
