using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientCompleteVisual : MonoBehaviour
{
    [Serializable]
    public struct IngredientGameObject
    {
        public KitchenObjectSO kitchenObjectSO;
        public GameObject ingredientGameObject;
    }

    [SerializeField] private PlateKitchenObject plateKitchenObject;
    [SerializeField] private List<IngredientGameObject> IngredientGameObjectList;

    private void Start()
    {
        plateKitchenObject.OnAddIngredient += PlateKitchenObject_OnAddIngredient;
    }

    private void PlateKitchenObject_OnAddIngredient(object sender, PlateKitchenObject.OnAddIngredientEventArgs e)
    {
        foreach(IngredientGameObject ingredient in IngredientGameObjectList)
        {
            if(ingredient.kitchenObjectSO == e.kitchenObjectSO)
            {
                ingredient.ingredientGameObject.SetActive(true);
            }
        }
    }
}
