using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateCounterVisual : MonoBehaviour
{
    [SerializeField] private PlateCounter plateCounter;
    [SerializeField] private Transform plateVisualPrefab;
    [SerializeField] private Transform counterTopPoint;
    private List<GameObject> plateVisualList;
    private void Awake()
    {
        plateVisualList = new List<GameObject>();
    }
    void Start()
    {
        plateCounter.OnPlateSpawned += PlateCounter_OnPlateSpawned;
        plateCounter.OnPlateRemoved += PlateCounter_OnPlateRemoved;
    }

    private void PlateCounter_OnPlateRemoved(object sender, System.EventArgs e)
    {
        GameObject plateVisualRemoved = plateVisualList[plateVisualList.Count - 1];
        plateVisualList.Remove(plateVisualRemoved);
        Destroy(plateVisualRemoved);
    }

    private void PlateCounter_OnPlateSpawned(object sender, System.EventArgs e)
    {
        Transform plateVisualTransform = Instantiate(plateVisualPrefab, counterTopPoint);

        plateVisualTransform.localPosition = new Vector3(0, plateVisualList.Count * 0.1f, 0);

        plateVisualList.Add(plateVisualTransform.gameObject);
    }
}
