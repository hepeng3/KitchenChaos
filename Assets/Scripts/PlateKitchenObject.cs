using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateKitchenObject : KitchenObject
{
    [SerializeField] private KitchenObjectSO[] validKitchenObjectSOArray;
    private List<KitchenObjectSO> ingredientList;
    public event EventHandler<OnAddIngredientEventArgs> OnAddIngredient;
    public class OnAddIngredientEventArgs : EventArgs
    {
        public KitchenObjectSO kitchenObjectSO;
    }
    private void Awake()
    {
        ingredientList = new List<KitchenObjectSO>();
    }
    public bool TryAddIngredient(KitchenObject ingredientKitchenObject)
    {
        foreach(KitchenObjectSO kitchenObjectSO in validKitchenObjectSOArray)
        {
            KitchenObjectSO ingredientKitchenObjectSO = ingredientKitchenObject.GetKitchenObjectSO();
            if (kitchenObjectSO == ingredientKitchenObjectSO)
            {
                if (!ingredientList.Contains(ingredientKitchenObjectSO))
                {
                    ingredientList.Add(ingredientKitchenObjectSO);
                    OnAddIngredient?.Invoke(this, new OnAddIngredientEventArgs
                    {
                        kitchenObjectSO = ingredientKitchenObjectSO
                    });
                    return true;
                }
            }
        }
        return false;
    }
    public List<KitchenObjectSO> GetIngredientList()
    {
        return ingredientList;
    }
}
