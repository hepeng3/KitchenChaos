using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryManager : MonoBehaviour
{
    [SerializeField] private RecipeSOListSO recipeSOListSO;
    private List<RecipeSO> spawnedRecipeList;

    private float spawnRecipeCountdownMax = 4f;
    private float spawnRecipeCountdown;
    private int spawnRecipeCountMax = 4;
    private int successfulDeliveryRecipeAmount;

    public event EventHandler OnDeliverySucceeded;
    public event EventHandler OnDeliveryFailed;
    public event EventHandler OnRecipeSpawned;
    public static DeliveryManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        spawnedRecipeList = new List<RecipeSO>();
    }
    private void Update()
    {
        if (KitchenChaosGameManager.Instance.IsPlaying())
        {
            spawnRecipeCountdown -= Time.deltaTime;
            if (spawnRecipeCountdown <= 0f && spawnedRecipeList.Count < spawnRecipeCountMax)
            {
                spawnRecipeCountdown = spawnRecipeCountdownMax;
                SpawnDelieryRecipe();
                OnRecipeSpawned?.Invoke(this, EventArgs.Empty);
            }
        }
    }
    private void SpawnDelieryRecipe()
    {
        List<RecipeSO> recipeSOList = recipeSOListSO.recipeSOList;
        RecipeSO recipeSO = recipeSOList[UnityEngine.Random.Range(0, recipeSOList.Count)];
        spawnedRecipeList.Add(recipeSO);
    }
    public void DeliveryRecipe(List<KitchenObjectSO> kitchenObjectSOList)
    {
        bool inSpawnedList = false;
        foreach (RecipeSO recipeSO in spawnedRecipeList)
        {
            List<KitchenObjectSO> spawnedKitchenObjectSOList = recipeSO.kitchenObjectSOList;
            if (spawnedKitchenObjectSOList.Count != kitchenObjectSOList.Count) continue;

            foreach (KitchenObjectSO kitchenObjectSO in kitchenObjectSOList)
            {
                //如果有重复配料会有问题
                if (spawnedKitchenObjectSOList.Contains(kitchenObjectSO))
                {
                    inSpawnedList = true;
                }
                else
                {
                    inSpawnedList = false;
                    continue;
                }
            }
            if (inSpawnedList)
            {
                successfulDeliveryRecipeAmount++;
                spawnedRecipeList.Remove(recipeSO);
                OnDeliverySucceeded?.Invoke(this, EventArgs.Empty);
                return;
            }
        }
        OnDeliveryFailed?.Invoke(this, EventArgs.Empty);
    }
    public List<RecipeSO> GetSpawnedRecipeList()
    {
        return spawnedRecipeList;
    }
    public int GetSuccessfulDeliveryRecipeAmount()
    {
        return successfulDeliveryRecipeAmount;
    }
}
