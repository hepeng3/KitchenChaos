using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] private SoundSO soundSO;
    public static SoundManager Instance { get; private set; }
    private const string SOUND_VOLUME = "SoundVolume";
    private float volume = 1.0f;
    // Start is called before the first frame update
    private void Awake()
    {
        Instance = this;

        volume = PlayerPrefs.GetFloat(SOUND_VOLUME, 1.0f);
    }
    void Start()
    {
        CuttingCounter.OnCut += PlayChopSound;
        TrashCounter.OnObjectTrashed += PlayTrashSound;
        DeliveryManager.Instance.OnDeliveryFailed += PlayDeliveryFailSound;
        DeliveryManager.Instance.OnDeliverySucceeded += PlayDeliverySuccessSound;
        BaseCounter.OnObjectPlaced += PlayObjectPlacedSound;
        Player.Instance.OnPickObject += PlayPickObjectSound;
    }
    public void ChangeVolume()
    {
        volume += 0.1f;
        if (volume >= 1.0f)
        {
            volume = .0f;
        }
        PlayerPrefs.SetFloat(SOUND_VOLUME, volume);
        PlayerPrefs.Save();
    }
    public float GetVolume()
    {
        return volume;
    }
    private void PlayChopSound(object sender, EventArgs e)
    {
        CuttingCounter cuttingCounter = sender as CuttingCounter;
        PlaySounds(soundSO.chop, cuttingCounter.transform.position, volume);
    }
    private void PlayTrashSound(object sender, EventArgs e)
    {
        TrashCounter trashCounter = sender as TrashCounter;
        PlaySounds(soundSO.trash, trashCounter.transform.position, volume);
    }
    private void PlayDeliveryFailSound(object sender, EventArgs e)
    {
        PlaySounds(soundSO.deliveryFail, DeliveryManager.Instance.transform.position, volume);
    }
    private void PlayDeliverySuccessSound(object sender, EventArgs e)
    {
        PlaySounds(soundSO.deliverySuccess, DeliveryManager.Instance.transform.position, volume);
    }
    private void PlayObjectPlacedSound(object sender, EventArgs e)
    {
        BaseCounter baseCounter = sender as BaseCounter;
        PlaySounds(soundSO.objectDrop, baseCounter.transform.position, volume);
    }
    private void PlayPickObjectSound(object sender, EventArgs e)
    {
        Player player = sender as Player;
        PlaySounds(soundSO.objectPickup, player.transform.position, volume);
    }
    public void PlayFootstepSound(Vector3 position)
    {
        PlaySounds(soundSO.footstep, position, volume);
    }
    public void PlayWarningSound(Vector3 position)
    {
        PlaySounds(soundSO.warning, position, volume);
    }
    private void PlaySounds(AudioClip[] audioClips, Vector3 position, float volume)
    {
        AudioClip audioClip = audioClips[UnityEngine.Random.Range(0, audioClips.Length - 1)];
        PlaySound(audioClip, position, volume);
    }
    private void PlaySound(AudioClip audioClip, Vector3 position, float volume)
    { 
        AudioSource.PlayClipAtPoint(audioClip, position, volume);
    }
}
