using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KitchenChaosGameManager : MonoBehaviour
{
    public static KitchenChaosGameManager Instance { get; private set; }
    public event EventHandler OnWaitingToCountdown;
    public event EventHandler OnGameOver;
    public event EventHandler OnGamePause;
    public event EventHandler OnGameUnPause;
    private float countdownTimer = 3.0f;
    private float playingClockTimer;
    private float playingClockTimerMax = 90.0f;
    private bool isGamePaused;
    //private float countdownTime;
    private GameState gameState;
    public enum GameState
    {
        WaitingStart,
        Countdown,
        Playing,
        GameOver
    }
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        //gameInput.OnInteractPerformed += GameInput_OnInteractPerformed;
        //gameInput.OnPausePerformed += GameInput_OnPausePerformed;
        GameInput.Instance.OnInteractPerformed += Instance_OnInteractPerformed;
        GameInput.Instance.OnPausePerformed += Instance_OnPausePerformed;
    }

    private void Instance_OnPausePerformed(object sender, EventArgs e)
    {
        TriggerGamePause();
    }

    private void Instance_OnInteractPerformed(object sender, EventArgs e)
    {
        if (gameState == GameState.WaitingStart)
        {
            gameState = GameState.Countdown;
            OnWaitingToCountdown?.Invoke(this, EventArgs.Empty);
        }
    }
    private void Update()
    {
        switch (gameState)
        {
            case GameState.WaitingStart:
                break;
            case GameState.Countdown:
                countdownTimer -= Time.deltaTime;
                if (countdownTimer <= .0f)
                {
                    gameState = GameState.Playing;
                }
                break;
            case GameState.Playing:
                playingClockTimer += Time.deltaTime;
                if (playingClockTimer >= playingClockTimerMax)
                {
                    gameState = GameState.GameOver;
                    OnGameOver?.Invoke(this, EventArgs.Empty);
                }
                break;
            case GameState.GameOver:
                break;
        }
    }
    public bool IsPlaying()
    {
        return gameState == GameState.Playing;
    }
    public int GetCountdownNumber()
    {
        return Mathf.CeilToInt(countdownTimer);
    }
    public float GetPlayingClockTimerFillAmount()
    {
        return playingClockTimer / playingClockTimerMax;
    }
    public void TriggerGamePause()
    {
        isGamePaused = !isGamePaused;
        if (isGamePaused)
        {
            Time.timeScale = .0f;
            OnGamePause?.Invoke(this, EventArgs.Empty);
        }
        else
        {
            Time.timeScale = 1.0f;
            OnGameUnPause?.Invoke(this, EventArgs.Empty);
        }
    }
}
