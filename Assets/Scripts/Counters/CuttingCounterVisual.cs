using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingCounterVisual : MonoBehaviour
{
    [SerializeField] private CuttingCounter cuttingCounter;
    private Animator animator;
    private const string CUT = "Cut";
    // Start is called before the first frame update
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    void Start()
    {
        cuttingCounter.OnCutAny += CuttingCounter_OnCutAny;
    }

    private void CuttingCounter_OnCutAny(object sender, System.EventArgs e)
    {
        animator.SetTrigger(CUT);
    }
}
