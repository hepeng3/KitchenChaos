using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="KichenObjectSO")]
public class KitchenObjectSO : ScriptableObject
{
    public string Objectname;
    public Sprite sprite;
    public Transform prefab;
}
