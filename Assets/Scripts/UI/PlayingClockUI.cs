using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayingClockUI : MonoBehaviour
{
    [SerializeField] private RectTransform clockUIProgress;
    private Image clockImage;
    // Start is called before the first frame update
    void Start()
    {
        clockImage = clockUIProgress.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        clockImage.fillAmount = KitchenChaosGameManager.Instance.GetPlayingClockTimerFillAmount();
    }
}
