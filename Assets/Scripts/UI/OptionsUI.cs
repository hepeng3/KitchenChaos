using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUI : MonoBehaviour
{
    [SerializeField] private Button soundManagerButton;
    [SerializeField] private TextMeshProUGUI soundManagerText;
    [SerializeField] private Button musicManagerButton;
    [SerializeField] private TextMeshProUGUI musicManagerText;
    [SerializeField] private Button closeButton;

    [SerializeField] private TextMeshProUGUI moveUpText;
    [SerializeField] private TextMeshProUGUI moveDownText;
    [SerializeField] private TextMeshProUGUI moveLeftText;
    [SerializeField] private TextMeshProUGUI moveRightText;
    [SerializeField] private TextMeshProUGUI keyInteractText;
    [SerializeField] private TextMeshProUGUI keyInteractAltText;
    [SerializeField] private TextMeshProUGUI keyPauseText;
    [SerializeField] private TextMeshProUGUI gamepadInteractText;
    [SerializeField] private TextMeshProUGUI gamepadInteractAltText;
    [SerializeField] private TextMeshProUGUI gamepadPauseText;
    [SerializeField] private Button moveUpButton;
    [SerializeField] private Button moveDownButton;
    [SerializeField] private Button moveLeftButton;
    [SerializeField] private Button moveRightButton;
    [SerializeField] private Button interactButton;
    [SerializeField] private Button interactAltButton;
    [SerializeField] private Button pauseButton;
    [SerializeField] private Button gamepadInteractButton;
    [SerializeField] private Button gamepadInteractAltButton;
    [SerializeField] private Button gamepadPauseButton;
    [SerializeField] private RectTransform pressToRebindKey;
    private Action OnOptionsUIClose;
    public static OptionsUI Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
        soundManagerButton.onClick.AddListener(() =>
        {
            SoundManager.Instance.ChangeVolume();
            UpdateVisual();
        });
        musicManagerButton.onClick.AddListener(() =>
        {
            MusicManager.Instance.ChangeVolume();
            UpdateVisual();
        });
        closeButton.onClick.AddListener(() =>
        {
            Hide();
            OnOptionsUIClose();
        });

        moveUpButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.MoveUp);
        });
        moveDownButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.MoveDown);
        });
        moveLeftButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.MoveLeft);
        });
        moveRightButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.MoveRight);
        });
        interactButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.Interact);
        });
        interactAltButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.InteractAlt);
        });
        pauseButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.Pause);
        });
        gamepadInteractButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.InteractGamepad);
        });
        gamepadInteractAltButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.InteractAltGamepad);
        });
        gamepadPauseButton.onClick.AddListener(() =>
        {
            RebindKey(GameInput.PlayerInputBinding.PauseGamepad);
        });
    }
    // Start is called before the first frame update
    void Start()
    {
        UpdateVisual();
        Hide();
    }
    private void UpdateVisual()
    {
        soundManagerText.text = "Sound Effects: " + Mathf.Round(SoundManager.Instance.GetVolume() * 10f);
        musicManagerText.text = "Music: " + Mathf.Round(MusicManager.Instance.GetVolume() * 10f);
        moveUpText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveUp);
        moveDownText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveDown);
        moveLeftText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveLeft);
        moveRightText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveRight);
        keyInteractText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.Interact);
        keyInteractAltText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractAlt);
        keyPauseText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.Pause);
        gamepadInteractText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractGamepad);
        gamepadInteractAltText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractAltGamepad);
        gamepadPauseText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.PauseGamepad);
    }
    private void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show(Action OnOptionsUIClose)
    {
        gameObject.SetActive(true);
        this.OnOptionsUIClose = OnOptionsUIClose;
        soundManagerButton.Select();
    }
    public void HidePressToRebindKey()
    {
        pressToRebindKey.gameObject.SetActive(false);
    }
    public void ShowPressToRebindKey()
    {
        pressToRebindKey.gameObject.SetActive(true);
    }
    private void RebindKey(GameInput.PlayerInputBinding playerInputBinding)
    {
        ShowPressToRebindKey();
        GameInput.Instance.RebindingInputAction(playerInputBinding, () =>
        {
            HidePressToRebindKey();
            UpdateVisual();
        });
    }
}
