using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSound : MonoBehaviour
{
    private Player player;
    private float playFootstepSoundTimer;
    private void Awake()
    {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        playFootstepSoundTimer -= Time.deltaTime;
        if(playFootstepSoundTimer <= .0f)
        {
            playFootstepSoundTimer = 0.1f;
            if (player.isWalking)
            {
                SoundManager.Instance.PlayFootstepSound(player.transform.position);
            }
        }
    }
}
