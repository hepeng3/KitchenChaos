using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoveCounter : BaseCounter, IHasProgress
{
    [SerializeField] private FryingRecipeSO[] fryingRecipeSOArray;
    [SerializeField] private BurningRecipeSO[] burningRecipeSOArray;
    private FryingRecipeSO fryingRecipeSO;
    private BurningRecipeSO burningRecipeSO;
    private float fryingProgress;
    private float burningProgress;

    public enum State
    {
        Idle,
        Frying,
        Burning,
        Burned
    }
    private State stoveCounterState;

    public event EventHandler<IHasProgress.OnProgressUpdateEventArgs> OnProgressUpdate;
    public event EventHandler<OnStateUpateEventArgs> OnStateUpate;
    public class OnStateUpateEventArgs : EventArgs
    {
        public State updatedState;
    }

    private void Update()
    {
        switch (stoveCounterState)
        {
            case State.Idle:
                break;
            case State.Frying:
                fryingProgress += Time.deltaTime;
                OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                {
                    progressNormalized = fryingProgress / fryingRecipeSO.fryingProgressMax
                });

                if (fryingProgress >= fryingRecipeSO.fryingProgressMax)
                {
                    GetKitchenObject().DestroySelf();
                    KitchenObject.SpawnKitchenObject(fryingRecipeSO.output, this);

                    fryingProgress = .0f;
                    this.fryingRecipeSO = null;

                    this.burningRecipeSO = GetBurningRecipeWithInput(GetKitchenObject().GetKitchenObjectSO());
                    stoveCounterState = State.Burning;
                    OnStateUpate?.Invoke(this, new OnStateUpateEventArgs
                    {
                        updatedState = stoveCounterState
                    });
                }
                break;
            case State.Burning:
                burningProgress += Time.deltaTime;
                OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                {
                    progressNormalized = burningProgress / burningRecipeSO.burningProgressMax
                });

                if (burningProgress >= burningRecipeSO.burningProgressMax)
                {
                    GetKitchenObject().DestroySelf();
                    KitchenObject.SpawnKitchenObject(burningRecipeSO.output, this);

                    burningProgress = .0f;
                    this.burningRecipeSO = null;

                    stoveCounterState = State.Burned;
                    OnStateUpate?.Invoke(this, new OnStateUpateEventArgs
                    {
                        updatedState = stoveCounterState
                    });
                    OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                    {
                        progressNormalized = .0f
                    });
                }
                break;
            case State.Burned:
                break;
        }
    }
    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            if (player.HasKitchenObject())
            {
                if (TryGetFryingRecipeWithInput(player.GetKitchenObject().GetKitchenObjectSO(), out FryingRecipeSO fryingRecipeSO))
                {
                    player.GetKitchenObject().SetKitchenObjectParent(this);
                    this.fryingRecipeSO = fryingRecipeSO;
                    stoveCounterState = State.Frying;
                    //fryingProgress = .0f;

                    OnStateUpate?.Invoke(this, new OnStateUpateEventArgs
                    {
                        updatedState = stoveCounterState
                    });
                    //OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                    //{
                    //    progressNormalized = fryingProgress / fryingRecipeSO.fryingProgressMax
                    //});
                }
            }
        }
        else
        {
            if (!player.HasKitchenObject())
            {
                GetKitchenObject().SetKitchenObjectParent(player);
                switch (stoveCounterState)
                {
                    case State.Frying:
                        fryingProgress = .0f;
                        this.fryingRecipeSO = null;
                        stoveCounterState = State.Idle;
                        break;
                    case State.Burning:
                        burningProgress = .0f;
                        this.burningRecipeSO = null;
                        stoveCounterState = State.Idle;
                        break;
                }
                OnStateUpate?.Invoke(this, new OnStateUpateEventArgs
                {
                    updatedState = stoveCounterState
                });
                OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                {
                    progressNormalized = .0f
                });
            }
            else
            {
                if (player.GetKitchenObject().TryGetPlateKitchenObject(out PlateKitchenObject plateKitchenObject))
                {
                    if (plateKitchenObject.TryAddIngredient(GetKitchenObject()))
                    {
                        GetKitchenObject().DestroySelf();
                        switch (stoveCounterState)
                        {
                            case State.Burning:
                                burningProgress = .0f;
                                this.burningRecipeSO = null;
                                stoveCounterState = State.Idle;
                                break;
                        }
                        OnStateUpate?.Invoke(this, new OnStateUpateEventArgs
                        {
                            updatedState = stoveCounterState
                        });
                        OnProgressUpdate?.Invoke(this, new IHasProgress.OnProgressUpdateEventArgs
                        {
                            progressNormalized = .0f
                        });
                    }
                }
            }
        }
    }
    private bool TryGetFryingRecipeWithInput(KitchenObjectSO inputKitchenObjectSO, out FryingRecipeSO fryingRecipeSO)
    {
        fryingRecipeSO = null;
        foreach(FryingRecipeSO recipeSO in fryingRecipeSOArray)
        {
            if(recipeSO.input == inputKitchenObjectSO)
            {
                fryingRecipeSO = recipeSO;
            }
        }
        return fryingRecipeSO != null;
    }
    private BurningRecipeSO GetBurningRecipeWithInput(KitchenObjectSO inputKitchenObjectSO)
    {
        //try?
        foreach (BurningRecipeSO recipeSO in burningRecipeSOArray)
        {
            if (recipeSO.input == inputKitchenObjectSO)
            {
                return recipeSO;
            }
        }
        return null;
    }
    public bool isBurning()
    {
        return stoveCounterState == State.Burning;
    }
}