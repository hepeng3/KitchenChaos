using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoveCounterSound : MonoBehaviour
{
    [SerializeField] private StoveCounter stoveCounter;
    private AudioSource audioSource;
    private bool playWarningSound;
    private float playWarningSoundTimer;
    private float playWarningSoundTimerMax = .3f;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        stoveCounter.OnStateUpate += StoveCounter_OnStateUpate;
        stoveCounter.OnProgressUpdate += StoveCounter_OnProgressUpdate;
    }

    private void StoveCounter_OnProgressUpdate(object sender, IHasProgress.OnProgressUpdateEventArgs e)
    {
        if (e.progressNormalized > .5f && stoveCounter.isBurning())
        {
            playWarningSound = true;
        }
        else
        {
            playWarningSound = false;
        }
    }

    private void StoveCounter_OnStateUpate(object sender, StoveCounter.OnStateUpateEventArgs e)
    {
        if(e.updatedState == StoveCounter.State.Frying || e.updatedState == StoveCounter.State.Burning)
        {
            audioSource.Play();
        }
        else
        {
            audioSource.Stop();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playWarningSound)
        {
            playWarningSoundTimer -= Time.deltaTime;
            if (playWarningSoundTimer <= .0f)
            {
                playWarningSoundTimer = playWarningSoundTimerMax;

                SoundManager.Instance.PlayWarningSound(stoveCounter.transform.position);
            }
        }
    }
}
