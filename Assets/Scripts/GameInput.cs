using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameInput : MonoBehaviour
{
    private PlayerInputActions playerInputActions;
    private const string PLAYER_PREFS_INPUT_BINDINGS = "InputBindings";
    public static GameInput Instance { get; private set; }
    public event EventHandler OnInteractPerformed;
    public event EventHandler OnInteractAlternatePerformed;
    public event EventHandler OnPausePerformed;
    public event EventHandler OnBindingRebind;
    private void Awake()
    {
        Instance = this;
        playerInputActions = new PlayerInputActions();

        if (PlayerPrefs.HasKey(PLAYER_PREFS_INPUT_BINDINGS))
        {
            playerInputActions.LoadBindingOverridesFromJson(PlayerPrefs.GetString(PLAYER_PREFS_INPUT_BINDINGS));
        }

        playerInputActions.Player.Enable();
        playerInputActions.Player.Interact.performed += Interact_performed;
        playerInputActions.Player.InteractAlternate.performed += InteractAlternate_performed;
        playerInputActions.Player.Pause.performed += Pause_performed;
    }
    private void OnDestroy()
    {
        playerInputActions.Player.Interact.performed -= Interact_performed;
        playerInputActions.Player.InteractAlternate.performed -= InteractAlternate_performed;
        playerInputActions.Player.Pause.performed -= Pause_performed;
        playerInputActions.Dispose();
    }

    public Vector2 GetMovementVector()
    {
        Vector2 vector = playerInputActions.Player.Move.ReadValue<Vector2>();
        return vector;
    }

    private void Interact_performed(InputAction.CallbackContext obj)
    {
        OnInteractPerformed?.Invoke(this, EventArgs.Empty);
    }
    private void InteractAlternate_performed(InputAction.CallbackContext obj)
    {
        OnInteractAlternatePerformed?.Invoke(this, EventArgs.Empty);
    }
    private void Pause_performed(InputAction.CallbackContext obj)
    {
        OnPausePerformed?.Invoke(this, EventArgs.Empty);
    }
    public enum PlayerInputBinding
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Interact,
        InteractAlt,
        Pause,
        InteractGamepad,
        InteractAltGamepad,
        PauseGamepad,
    }
    public string GetPlayerInputBindingText(PlayerInputBinding playerInputBinding)
    {
        string bindingText = "";
        switch (playerInputBinding)
        {
            case PlayerInputBinding.MoveUp:
                bindingText = playerInputActions.Player.Move.bindings[1].ToDisplayString();
                break;
            case PlayerInputBinding.MoveDown:
                bindingText = playerInputActions.Player.Move.bindings[2].ToDisplayString();
                break;
            case PlayerInputBinding.MoveLeft:
                bindingText = playerInputActions.Player.Move.bindings[3].ToDisplayString();
                break;
            case PlayerInputBinding.MoveRight:
                bindingText = playerInputActions.Player.Move.bindings[4].ToDisplayString();
                break;
            case PlayerInputBinding.Interact:
                bindingText = playerInputActions.Player.Interact.bindings[0].ToDisplayString();
                break;
            case PlayerInputBinding.InteractAlt:
                bindingText = playerInputActions.Player.InteractAlternate.bindings[0].ToDisplayString();
                break;
            case PlayerInputBinding.Pause:
                bindingText = playerInputActions.Player.Pause.bindings[0].ToDisplayString();
                break;
            case PlayerInputBinding.InteractGamepad:
                bindingText = playerInputActions.Player.Interact.bindings[1].ToDisplayString();
                break;
            case PlayerInputBinding.InteractAltGamepad:
                bindingText = playerInputActions.Player.InteractAlternate.bindings[1].ToDisplayString();
                break;
            case PlayerInputBinding.PauseGamepad:
                bindingText = playerInputActions.Player.Pause.bindings[1].ToDisplayString();
                break;
        }
        return bindingText;
    }
    public void RebindingInputAction(PlayerInputBinding playerInputBinding, Action onBound)
    {
        InputAction inputAction;

        int bindingIndex;

        playerInputActions.Player.Disable();

        switch (playerInputBinding)
        {
            default:
            case PlayerInputBinding.MoveUp:
                bindingIndex = 1;
                inputAction = playerInputActions.Player.Move;
                break;
            case PlayerInputBinding.MoveDown:
                bindingIndex = 2;
                inputAction = playerInputActions.Player.Move;
                break;
            case PlayerInputBinding.MoveLeft:
                bindingIndex = 3;
                inputAction = playerInputActions.Player.Move;
                break;
            case PlayerInputBinding.MoveRight:
                bindingIndex = 4;
                inputAction = playerInputActions.Player.Move;
                break;
            case PlayerInputBinding.Interact:
                bindingIndex = 0;
                inputAction = playerInputActions.Player.Interact;
                break;
            case PlayerInputBinding.InteractAlt:
                bindingIndex = 0;
                inputAction = playerInputActions.Player.InteractAlternate;
                break;
            case PlayerInputBinding.Pause:
                bindingIndex = 0;
                inputAction = playerInputActions.Player.Pause;
                break;
            case PlayerInputBinding.InteractGamepad:
                bindingIndex = 1;
                inputAction = playerInputActions.Player.Interact;
                break;
            case PlayerInputBinding.InteractAltGamepad:
                bindingIndex = 1;
                inputAction = playerInputActions.Player.InteractAlternate;
                break;
            case PlayerInputBinding.PauseGamepad:
                bindingIndex = 1;
                inputAction = playerInputActions.Player.Pause;
                break;
        }

        inputAction.PerformInteractiveRebinding(bindingIndex).OnComplete(callback =>
        {
            callback.Dispose();//?
            playerInputActions.Player.Enable();
            onBound();

            PlayerPrefs.SetString(PLAYER_PREFS_INPUT_BINDINGS, playerInputActions.SaveBindingOverridesAsJson());
            PlayerPrefs.Save();

            OnBindingRebind?.Invoke(this, EventArgs.Empty);
        }).Start();
    }
}
