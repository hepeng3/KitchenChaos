using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="RecipeSOListSO")]
public class RecipeSOListSO : ScriptableObject
{
    public List<RecipeSO> recipeSOList;
}
