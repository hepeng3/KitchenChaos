using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TutorialUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI moveUpText;
    [SerializeField] private TextMeshProUGUI moveDownText;
    [SerializeField] private TextMeshProUGUI moveLeftText;
    [SerializeField] private TextMeshProUGUI moveRightText;
    [SerializeField] private TextMeshProUGUI keyInteractText;
    [SerializeField] private TextMeshProUGUI keyInteractAltText;
    [SerializeField] private TextMeshProUGUI keyPauseText;
    [SerializeField] private TextMeshProUGUI gamepadInteractText;
    [SerializeField] private TextMeshProUGUI gamepadInteractAltText;
    [SerializeField] private TextMeshProUGUI gamepadPauseText;
    // Start is called before the first frame update
    void Start()
    {
        KitchenChaosGameManager.Instance.OnWaitingToCountdown += Instance_OnWaitingToCountdown;
        GameInput.Instance.OnBindingRebind += Instance_OnBindingRebind;
        updateVisual();
    }

    private void Instance_OnBindingRebind(object sender, System.EventArgs e)
    {
        updateVisual();
    }

    private void Instance_OnWaitingToCountdown(object sender, System.EventArgs e)
    {
        Hide();
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }
    private void Show()
    {
        gameObject.SetActive(true);
    }
    private void updateVisual()
    {
        moveUpText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveUp);
        moveDownText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveDown);
        moveLeftText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveLeft);
        moveRightText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.MoveRight);
        keyInteractText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.Interact);
        keyInteractAltText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractAlt);
        keyPauseText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.Pause);
        gamepadInteractText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractGamepad);
        gamepadInteractAltText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.InteractAltGamepad);
        gamepadPauseText.text = GameInput.Instance.GetPlayerInputBindingText(GameInput.PlayerInputBinding.PauseGamepad);
    }
}
