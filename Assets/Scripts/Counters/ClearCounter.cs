using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearCounter : BaseCounter
{
    public override void Interact(Player player)
    {
        if (!HasKitchenObject())
        {
            if (player.HasKitchenObject())
            {
                player.GetKitchenObject().SetKitchenObjectParent(this);
            }
        }
        else
        {
            if (!player.HasKitchenObject())
            {
                GetKitchenObject().SetKitchenObjectParent(player);
            }
            else
            {
                PlateKitchenObject plateKitchenObject;
                if(GetKitchenObject().TryGetPlateKitchenObject(out plateKitchenObject))
                {
                    if (plateKitchenObject.TryAddIngredient(player.GetKitchenObject()))
                    {
                        player.GetKitchenObject().DestroySelf();
                    }
                }
                else
                {
                    if(player.GetKitchenObject().TryGetPlateKitchenObject(out plateKitchenObject))
                    {
                        if (plateKitchenObject.TryAddIngredient(GetKitchenObject()))
                        {
                            GetKitchenObject().DestroySelf();
                        }
                    }
                }
            }
        }
    }
}
