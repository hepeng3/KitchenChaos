using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WarningUI : MonoBehaviour
{
    [SerializeField] private StoveCounter stoveCounter;
    private void Start()
    {
        gameObject.SetActive(false);
        stoveCounter.OnProgressUpdate += StoveCounter_OnProgressUpdate;
    }

    private void StoveCounter_OnProgressUpdate(object sender, IHasProgress.OnProgressUpdateEventArgs e)
    {
        if (e.progressNormalized >= .5f && stoveCounter.isBurning())
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
