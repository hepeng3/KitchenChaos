using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectedCounterVisual : MonoBehaviour
{
    [SerializeField] private BaseCounter baseCounter;
    [SerializeField] private GameObject[] visualGameObjectArray;
    // Start is called before the first frame update
    void Start()
    {
        Player.Instance.OnSelectedCounterChanged += OnSelectedCounterChangedHandler;
    }

    private void OnDestroy()
    {
        //Debug.Log("Destory select counter, unsub event");
        Player.Instance.OnSelectedCounterChanged -= OnSelectedCounterChangedHandler;
    }

    private void OnSelectedCounterChangedHandler(object sender, Player.OnSelectedCounterChangedEventArgs e)
    {
        //Debug.Log("select counter changed");
        if(e.selectedCounter == baseCounter)
        {
            Show();
        }
        else
        {
            Hide();
        }
    }

    private void Show()
    {
        foreach(GameObject child in visualGameObjectArray)
        {
            child.SetActive(true);
        }
    }
    private void Hide()
    {
        foreach(GameObject child in visualGameObjectArray)
        {
            child.SetActive(false);
        }
    }
}
