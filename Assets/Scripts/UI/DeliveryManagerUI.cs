using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeliveryManagerUI : MonoBehaviour
{
    [SerializeField] private RectTransform DeliveryRecipeContainerTemplate;
    [SerializeField] private RectTransform DeliveryContainer;
    // Start is called before the first frame update
    void Start()
    {
        DeliveryManager.Instance.OnRecipeSpawned += Instance_OnRecipeSpawned;
    }

    private void Instance_OnRecipeSpawned(object sender, System.EventArgs e)
    {
        foreach(RectTransform child in DeliveryContainer)
        {
            if (child != DeliveryRecipeContainerTemplate)
            {
                Destroy(child.gameObject);
            }
        }
        List<RecipeSO> recipeSOList = DeliveryManager.Instance.GetSpawnedRecipeList();

        foreach(RecipeSO recipeSO in recipeSOList)
        {
            RectTransform deliveryRecipeContainer = Instantiate(DeliveryRecipeContainerTemplate, DeliveryContainer);
            deliveryRecipeContainer.gameObject.SetActive(true);
            deliveryRecipeContainer.GetComponent<DeliveryRecipeContainerUI>().SetSingleRecipe(recipeSO);
        }
    }
}
