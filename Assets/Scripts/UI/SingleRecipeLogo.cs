using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleRecipeLogo : MonoBehaviour
{
    [SerializeField] private Image image;

    public void SetRecipeLogo(KitchenObjectSO kitchenObjectSO)
    {
        image.sprite = kitchenObjectSO.sprite;
    }
}
