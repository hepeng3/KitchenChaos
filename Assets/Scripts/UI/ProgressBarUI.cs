using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
    [SerializeField] private GameObject hasProgressGameObject;
    [SerializeField] private Image barImage;
    private IHasProgress hasProgress;
    private void Awake()
    {
        hasProgress = hasProgressGameObject.GetComponent<IHasProgress>();
    }
    private void Start()
    {
        gameObject.SetActive(false);
        hasProgress.OnProgressUpdate += HasProgress_OnProgressUpdate;
    }

    private void HasProgress_OnProgressUpdate(object sender, IHasProgress.OnProgressUpdateEventArgs e)
    {
        barImage.fillAmount = e.progressNormalized;
        if (barImage.fillAmount >= 1.0f || barImage.fillAmount <= .0f)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
    }
}
