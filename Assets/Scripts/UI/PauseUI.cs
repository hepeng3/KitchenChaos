using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : MonoBehaviour
{
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button optionButton;
    [SerializeField] private Button mainMenuButton;
    // Start is called before the first frame update
    private void Awake()
    {
        resumeButton.onClick.AddListener(() =>
        {
            KitchenChaosGameManager.Instance.TriggerGamePause();
        });
        optionButton.onClick.AddListener(() =>
        {
            Hide();
            OptionsUI.Instance.Show(Show);
        });
        mainMenuButton.onClick.AddListener(() =>
        {
            Loader.Load(Loader.Scene.MainMenuScene);
        });
    }
    void Start()
    {
        KitchenChaosGameManager.Instance.OnGamePause += GameManagerInstance_OnGamePause;
        KitchenChaosGameManager.Instance.OnGameUnPause += GameManagerInstance_OnGameUnPause;
        Hide();
    }

    private void GameManagerInstance_OnGameUnPause(object sender, System.EventArgs e)
    {
        Hide();
    }

    private void GameManagerInstance_OnGamePause(object sender, System.EventArgs e)
    {
        Show();
    }

    // Update is called once per frame
    private void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        resumeButton.Select();
        gameObject.SetActive(true);
    }
}
